# Alkemy warmup challenge

This project was created with create-react-app as an alkemy challenge.

## Steps to run this project

1. You need to have NodeJs last stable version installed on your system.
2. Clone this project.
3. Step onto the root folder.
4. Run `npm install` command to install the project's dependencies.
5. Run `npm start` commando to run the project on port 3000.

