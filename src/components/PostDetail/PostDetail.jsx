import { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Modal } from './components/Modal';

const getURL = (id) => `https://jsonplaceholder.typicode.com/posts/${id}`;

export const PostDetail = () => {
  
  // Capturo el id que viene por parametros
  const { id } = useParams();

  // Estado
  const [post, setPost] = useState({});

  // history para redireccionar
  const history = useHistory();

  // GET al detalle por id
  useEffect(() => {
    axios.get(getURL(id))
      .then(fetchedData => setPost(fetchedData.data))
      .catch(e => console.log(e))
  }, [id])

  // DELETE por id
  const deletePost = () => {
    axios.delete(getURL(id))
      .then(response => {
        console.log(response);
        response.status === 200 && history.push('/home');
      })
      .catch(e => console.log(e))
  }

  return (
    <div className="card col-10 col-sm-8 col-md-6 col-lg-4 mx-auto mt-4 p-5 bg-light bg-gradient">
      <div className="card-body">
        <h5 className="card-title">{post.title}</h5>
        <p className="card-text">{post.body}</p>
        <div className="w-100 d-flex justify-content-between">
          <Link to={`/home`} className="btn btn-primary">Return</Link>
          <Modal delete={deletePost} />
        </div>
      </div>
    </div>
  )
}
