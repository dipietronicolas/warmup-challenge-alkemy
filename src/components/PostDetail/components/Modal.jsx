export const Modal = (props) => {
  return (
    <>
      {/*<!-- Button trigger modal -->*/}
      <button 
        type="button" 
        className="btn btn-danger" 
        data-bs-toggle="modal" 
        data-bs-target="#staticBackdrop">
        Delete post
      </button>

      {/*<!-- Modal -->*/}
      <div 
        className="modal fade" 
        id="staticBackdrop" 
        data-bs-backdrop="static" 
        data-bs-keyboard="false" 
        tabIndex="-1" 
        aria-labelledby="staticBackdropLabel" 
        aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 
                className="modal-title" 
                id="staticBackdropLabel">Delete post</h5>
              <button 
                type="button" 
                className="btn-close" 
                data-bs-dismiss="modal" 
                aria-label="Close"></button>
            </div>
            <div className="modal-body">
              Are you sure you want to delete this post?
            </div>
            <div 
              className="modal-footer">
              <button 
                type="button" 
                className="btn btn-secondary" 
                data-bs-dismiss="modal">Close</button>
              <button 
                type="button" 
                className="btn btn-warning"
                data-bs-dismiss="modal"
                onClick={() => props.delete()}>Yes, I'm sure</button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
