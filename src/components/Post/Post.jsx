import React from 'react';
import { Link } from 'react-router-dom';
import './Post.css';
import 'bootstrap-icons/font/bootstrap-icons.css';

export const Post = ({ post }) => {
  return (
    <div className="card myCard my-2 col-10 col-sm-5 col-md-4 col-xl-3">
      <div className="card-body cardBody p-0 position-relative">
        <Link className="position-absolute text-white end-0 p-1" to={`edit/${post.id}`}>
          <i className="bi bi-pencil-square"></i>
        </Link>
        <h5 className="card-img-top cardTitle bg-dark bg-gradient text-light p-3 pr-5 display-4 fs-5 text-center">{post.title}</h5>
        <p className="card-text p-3 text-start">{post.body}</p>
        <Link to={`/detail/${post.id}`} className="btn btn-warning w-100  ">Ver detalle</Link>
      </div>
    </div>
  )
}
