import axios from 'axios';
import { useEffect, useState, useContext } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { UserContext } from '../../context/UserContext';
import { useFormik } from 'formik';

export const EditForm = () => {

  // ID del post a editar
  const { id } = useParams();

  // Post actual
  const [currentPost, setCurrentPost] = useState(undefined);

  // Variable que es true si el usuario esta loggeado
  const { getLog } = useContext(UserContext);

  // history para redireccionar
  const history = useHistory();

  // Manejo del form con formik
  const { handleSubmit, handleChange, values } = useFormik({
    initialValues: {
      title: currentPost ? currentPost.title : '',
      body: currentPost ? currentPost.title : ''
    },
    enableReinitialize: true,
    onSubmit: values => {
      edit(values.title, values.body);
    }
  });

  // Function que edita un post
  const edit = (title, body) => {
    axios.put(`https://jsonplaceholder.typicode.com/posts/${id}`, {
      title, body
    })
      .then(response => {
        console.log(response.data);
        setCurrentPost(response.data);
      })
      .catch(e => console.log(e))
  }

  // Function que hace un get por id
  useEffect(() => {
    getLog()
      ? (
        axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
          .then(response => {
            setCurrentPost(response.data);
          })
          .catch(e => console.log(e))
      ) : history.push('/')
      // eslint-disable-next-line
  }, [id])

  return (
    <div>
      <form className="card col-10 col-sm-8 col-md-6 col-lg-4 mx-auto mt-4 p-5 bg-light bg-gradient" onSubmit={handleSubmit}>
        <h4 className="display-4 fs-3 mb-4">Edit a post</h4>
        <div className="mb-3">
          <label
            className="form-label">Title</label>
          <input
            type="text"
            name="title"
            className="form-control"
            value={values.title}
            onChange={handleChange}
            autoFocus />
        </div>
        <div className="mb-3">
          <label
            className="form-label">Content</label>
          <textarea
            name="body"
            className="form-control"
            value={values.body}
            onChange={handleChange} />
        </div>
        <button
          type="submit"
          className="btn btn-outline-primary">Submit</button>
      </form>
    </div>
  )
}
