import { useEffect, useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { UserContext } from '../../context/UserContext';
import { Post } from '../Post/Post';
import axios from 'axios';

export const PostContainer = () => {

  // Estado que guarda los post
  const [posts, setPosts] = useState([]);

  // Variable que es true si el usuario esta loggeado
  const { isLogged } = useContext(UserContext);

  // Variable que uso para redirigir
  const history = useHistory();

  // Get a la api y traigo los posts.
  useEffect(() => {
    isLogged
      ? axios.get('https://jsonplaceholder.typicode.com/posts')
          .then(fetchedData => setPosts(fetchedData.data))
          .catch(error => console.log(error))
      : history.push('/')
  }, [isLogged, history])


  return (
    <div className="posts-container col-11 d-flex flex-wrap justify-content-center align-items-center pt-4 mx-auto">
      {
        posts.map((post) => {
          return <Post post={post} key={post.id} />
        })
      }
    </div>
  )
}
