import { useState, useEffect, useContext } from 'react';
import { useFormik } from 'formik';
import { UserContext } from '../../context/UserContext';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

export const AddPostForm = () => {

  // Setteo el nuevo post, esto es parte de la simulacion de agregar 
  // un nuevo post, para este ejercicio no tiene mucho sentido, pero 
  // estamos simulando asi que lo guardo en un esado
  const [newPost, setNewPost] = useState(null);

  // Variable que es true si el usuario esta loggeado
  const { getLog } = useContext(UserContext);

  // history para redireccionar
  const history = useHistory();

  // Manejo del form con formik
  const { handleSubmit, handleChange, values } = useFormik({
    initialValues: {
      title: '',
      body: ''
    },
    onSubmit: values => {
      post(values.title, values.body);
    },
  });

  // Function que hace el post a la api
  const post = (title, body) => {
    axios.post('https://jsonplaceholder.typicode.com/posts', {
      title, body
    })
      .then(fetchedData => {
        console.log(fetchedData);
        setNewPost(fetchedData.data);
      })
      .catch(error => console.log(error))
  }

  // Validar que este loggeado
  useEffect(() => {
    !getLog()
      && history.push('/'); 
  })

  return (
    <div>
      <form className="card col-10 col-sm-8 col-md-6 col-lg-4 mx-auto mt-4 p-5 bg-light bg-gradient" onSubmit={handleSubmit}>
        <h4 className="display-4 fs-3 mb-4">Add a Post</h4>
        <div className="mb-3">
          <label
            className="form-label">Title</label>
          <input
            type="text"
            name="title"
            className="form-control"
            value={values.title}
            onChange={handleChange}
            autoFocus />
        </div>
        <div className="mb-3">
          <label
            className="form-label">Content</label>
          <textarea
            name="body"
            className="form-control"
            value={values.body}
            onChange={handleChange} />
        </div>
        <button
          type="submit"
          className="btn btn-outline-primary">Submit</button>
      </form>
      {
        newPost
        && (
          <div
            className="alert alert-success alert-dismissible fade show mt-3"
            role="alert">
            New post created
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="alert"
              aria-label="Close"></button>
          </div>
        )
      }
    </div>
  )
}
