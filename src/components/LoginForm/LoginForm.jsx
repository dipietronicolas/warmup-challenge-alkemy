import { useContext, useEffect } from 'react';
import { useFormik } from 'formik';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import { UserContext } from '../../context/UserContext';

export const LoginForm = () => {

  const history = useHistory();

  const { login, isLogged } = useContext(UserContext);

  const { handleSubmit, handleChange, values } = useFormik({
    initialValues: {
      email: '',
      password: ''
    },
    onSubmit: values => {
      getAuthToken(values.email, values.password);
    },
  });
  // URL: http://challenge-react.alkemy.org/
  // email: challenge@alkemy.org
  // password: react
  const getAuthToken = (email, password) => {
    axios.post('http://challenge-react.alkemy.org/', {
      email: email,
      password: password
    })
      .then(response => {
        login(response.data.token);
        // localStorage.setItem('token', response.data.token);
        // getLog() && history.push('/home');
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  useEffect(() => {
    isLogged && history.push('/home');
  }, [isLogged, history])

  return (
    <form className="card col-10 col-sm-8 col-md-6 col-lg-4 mx-auto mt-4 p-5 bg-light bg-gradient" onSubmit={handleSubmit}>
      <h2 className="display-4 mb-4">Login</h2>
      <div className="mb-3">
        <label
          className="form-label">Email address</label>
        <input
          type="email"
          name="email"
          className="form-control"
          value={values.email}
          onChange={handleChange}
          autoFocus />
      </div>
      <div className="mb-3">
        <label
          className="form-label">Password</label>
        <input
          type="password"
          name="password"
          className="form-control"
          value={values.password}
          onChange={handleChange} />
      </div>
      <button
        type="submit"
        className="btn btn-outline-primary">Submit</button>
    </form>
  )
}
