import { useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { UserContext } from '../../context/UserContext';

export const Navigation = () => {

  const { logout } = useContext(UserContext);

  return (
    <div className="jumbotron p-4 col-10 col-sm-8 mx-auto text-dark text-center alert-light">
      <Link 
        className="display-2 mb-4 text-decoration-none text-dark" 
        to="/">postApp</Link>
      <nav className="navbar navbar-expand-lg navbar-light alert-secondary col-12 col-lg-11 mx-auto my-4">
        <div className="container-fluid">
          <div className="navbar-nav col-12 col-sm-8 mx-auto d-flex flex-column flex-sm-row justify-content-between">
            <NavLink 
              className="nav-link" 
              aria-current="page" 
              to="/home">Home</NavLink>
            <NavLink 
              className="nav-link" 
              to="/create">Add a Post</NavLink>
            <button 
              className="nav-link bg-transparent border-0 p-1" 
              onClick={() => logout()}>Logout</button>
          </div>
        </div>
      </nav>
    </div>
  )
}
