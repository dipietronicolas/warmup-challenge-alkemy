import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { AddPostForm } from '../components/AddPostForm/AddPostForm';
import { EditForm } from '../components/EditForm/EditForm';
import { LoginForm } from '../components/LoginForm/LoginForm';
import { Navigation } from '../components/Navigation/Navigation';
import { PostContainer } from '../components/PostContainer/PostContainer';
import { PostDetail } from '../components/PostDetail/PostDetail';

export const Router = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <LoginForm />
        </Route>
        <Route path="/home">
          <Navigation />
          <PostContainer />
        </Route>
        <Route path="/create">
          <Navigation />
          <AddPostForm />
        </Route>
        <Route path="/detail/:id">
          <Navigation />
          <PostDetail />
        </Route>
        <Route path="/edit/:id">
          <Navigation />
          <EditForm />
        </Route>
      </Switch>
    </BrowserRouter>
  )
}