import { UserProvider } from './context/UserContext';
import { Router } from './router/Router';
import './App.css';

function App() {
  return (
    <UserProvider>
      <div className="App col-12 mx-auto p-4 alert-warning bg-gradient">
        <Router />
      </div>
    </UserProvider>
  );
}

export default App;
