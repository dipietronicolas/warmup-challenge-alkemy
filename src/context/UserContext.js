import { createContext, useState } from 'react';

export const UserContext = createContext();

export const UserProvider = ({ children }) => {

  const [isLogged, setIsLogged] = useState(null);
  
  const login = (token) => {
    localStorage.setItem('token', token);
    setIsLogged(true);
  }

  const logout = () => {
    console.log("logout");
    localStorage.setItem('token', '');
    setIsLogged(false);
  }

  const getLog = () => {
    if(localStorage.getItem("token")){
      return true
    } else {
      setIsLogged(false);
      return false;
    }
  }
  

  return (
    <UserContext.Provider value={{
      isLogged,
      logout,
      login,
      getLog
    }}>
      { children }
    </UserContext.Provider>
  )
}
